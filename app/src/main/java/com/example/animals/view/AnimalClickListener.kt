package com.example.animals.view

import android.view.View

interface AnimalClickListener {

    fun onAnimalClicked(v: View)
}