package com.example.animals.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.provider.Settings.Global.putLong
import androidx.core.content.edit

class SharedPreferencesHelper {

    companion object {

        private const val PREF_API_KEY = "Pref key"
        private var prefs: SharedPreferences? = null

        @Volatile
        private var instance: SharedPreferencesHelper? = null
        private val LOCK = Any()

        operator fun invoke(context: Context): SharedPreferencesHelper =
            instance ?: synchronized(LOCK) {
                instance ?: buildHelper(context).also {
                    instance = it
                }
            }

        private fun buildHelper(context: Context): SharedPreferencesHelper {
            prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return SharedPreferencesHelper()
        }

    }


    fun saveApiKey(key: String)
    {
        prefs?.edit(commit=true){
            putString(PREF_API_KEY, key)?.apply()

        }
    }

    fun getApiKey() = prefs?.getString(PREF_API_KEY,null)
}
